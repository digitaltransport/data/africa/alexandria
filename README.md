# Alexandria (Egypt)
This repository contains all documentation and datasets produced throughout the DT4A Capacity building and mapping initiative done for the city of Alexandria in 2022.


## Attribution
This dataset is a product of a collaborative effort between [DigitalTransport4Africa (DT4A)](https://digitaltransport4africa.org/ "https://digitaltransport4africa.org/") and [Transport for Cairo (TfC)](https://transportforcairo.com/ "https://transportforcairo.com/"), funded by [French Development Agency (AfD)](https://www.afd.fr/en). The data collection was conducted by 80 students from the [Arab Academy for Science, Technology & Maritime Transport](https://aast.edu/en/ "https://aast.edu/en/"). The data collection was conducted across 104 transit routes in Alexandria city from November 14th to December 15th, 2022.
## License
The dataset is licensed under [Creative Commons Attribution-NonCommercial 4.0 International (CC BY-NC 4.0) License](https://creativecommons.org/licenses/by-nc/4.0/ "https://creativecommons.org/licenses/by-nc/4.0/"). For data usage, please clearly provide attribution and indicate if any modifications were made. Please contact us at  [info@digitaltransport4africa.org](mailto:info@digitaltransport4africa.org)  to request additional permissions.